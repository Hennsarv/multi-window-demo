﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiWindowDemo
{
    /// <summary>
    /// Interaction logic for Kategooriad.xaml
    /// </summary>
    public partial class Kategooriad : Window
    {
        public Kategooriad()
        {
            InitializeComponent();
        }

        private void Form_Loaded(object sender, RoutedEventArgs e)
        {
            this.TabelCategories.ItemsSource = App.ne.Categories.ToList();
        }

        private void Tooted_Click(object sender, RoutedEventArgs e)
        {
            if (this.TabelCategories.SelectedItem != null)
            {
                (new Tooted() { CategoryId = ((Category)(TabelCategories.SelectedValue)).CategoryID }).Show();
            }
        }
    }
}

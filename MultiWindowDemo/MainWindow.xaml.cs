﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiWindowDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Click_Kasutajad(object sender, RoutedEventArgs e)
        {
            Kasutajad k = new Kasutajad();
            this.Hide();
            k.ShowDialog();
            this.Show();
        }

        private void Click_Tooted(object sender, RoutedEventArgs e)
        {
            (new Tooted()).Show();
        }

        private void Click_Kategooriad(object sender, RoutedEventArgs e)
        {
            (new Kategooriad()).Show();
        }
    }
}

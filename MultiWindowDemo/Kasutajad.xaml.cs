﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiWindowDemo
{
    /// <summary>
    /// Interaction logic for Kasutajad.xaml
    /// </summary>
    partial class Employee
    {
        public string FullName
        {
            get => $"{FirstName} {LastName}";
        }
    }

    public partial class Kasutajad : Window
    {
        public int employeeID = 0;
        public Kasutajad()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.TabelEmployees.ItemsSource =
                App.ne.Employees
                .Where(x => employeeID == 0 || x.ReportsTo == employeeID)
                .ToList();
        }

        private void Emp_Selected(object sender, RoutedEventArgs e)
        {
            if (TabelEmployees.SelectedValue != null)
            { 
            employeeID = ((Employee)(TabelEmployees.SelectedValue)).EmployeeID;
            Window_Loaded(sender, e);
            }
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            employeeID = 0;
            Window_Loaded(sender, e);

        }

        private void Tooted_Click(object sender, RoutedEventArgs e)
        {
            (   new Tooted()
                {
                    UserName = (App.ne.Employees
                    .Where(x => x.EmployeeID == this.employeeID)
                    .SingleOrDefault()?.FullName)??""
                }
            ).Show();
        }
    }
}

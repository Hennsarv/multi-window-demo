﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiWindowDemo
{
    /// <summary>
    /// Interaction logic for Tooted.xaml
    /// </summary>
    public partial class Tooted : Window
    {
        public string UserName = "tundmatu";
        public int CategoryId = 0;
        public Tooted()
        {
            InitializeComponent();

        }

        private void Form_Loaded(object sender, RoutedEventArgs e)
        {
            this.TabelProducts.ItemsSource = App.ne.Products
                .Where(x => this.CategoryId == 0 || x.CategoryID == this.CategoryId)
                .ToList();
            this.UserNameText.Text = UserName;
        }
    }
}
